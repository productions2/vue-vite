import { PrimeIcons } from "primevue/api";
export type footerType = {
  title: string;
  children: {
    icon?: string;
    title: string;
  }[];
}[];

export const footerData: footerType = [
  {
    title: "about",
    children: [
      { title: "History" },
      { title: "Our Team" },
      { title: "Brand Guidelines" },
      { title: "Terms & Condition" },
      { title: "Privacy Policy" },
    ],
  },
  {
    title: "Services",
    children: [
      { title: "How to Order" },
      { title: "Our Product" },
      { title: "Order Status" },
      { title: "Promo" },
      { title: "Payment Method" },
    ],
  },
  {
    title: "follow",
    children: [
      {
        icon: PrimeIcons.FACEBOOK,
        title: "Facebook",
      },
      {
        icon: PrimeIcons.TWITTER,
        title: "Twitter",
      },
      {
        icon: PrimeIcons.INSTAGRAM,
        title: "Instagram",
      },
      {
        icon: PrimeIcons.WHATSAPP,
        title: "Whatsaap",
      },
    ],
  },
];
