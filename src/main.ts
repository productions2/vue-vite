import { createApp } from "vue";
import "./style.css";
import "/node_modules/primeflex/primeflex.css";

import "primeicons/primeicons.css";

import App from "./App.vue";


const app = createApp(App);
app.mount("#app");
